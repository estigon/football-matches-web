import React, { useState, useEffect } from 'react';
import { DataGrid } from '@mui/x-data-grid';

const COLUMNS = [
    { field: 'id', headerName: 'ID', minWidth: 90, flex: 1 },
    { field: 'date', headerName: 'DATE', minWidth: 120, flex: 1 },
    { field: 'winner', headerName: 'WINNER', minWidth: 150, flex: 1 },
    { field: 'score', headerName: 'SCORE', minWidth: 90, flex: 1 },
    { field: 'awayTeam', headerName: 'AWAY TEAM', minWidth: 200, flex: 1 },
    { field: 'homeTeam', headerName: 'HOME TEAM', minWidth: 200, flex: 1 },
    { field: 'matchStatus', headerName: 'MATCH STATUS', minWidth: 150, flex: 1 },

];

const BASE_URL = 'http://localhost:4000';

export default function TableMatches() {
    const [tableData, setTableData] = useState([]);
    const [pageSize, setPageSize] = useState(50);
    const [pageNumber, setPageNumber] = useState(0);
    const [rowCountState, setRowCountState] = useState(0);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        fetch(`${BASE_URL}/api/match?limit=${pageSize}&offset=${(pageNumber === 0) ? 1 : ((pageNumber * pageSize) + 1)}`)
            .then((data) => data.json())
            .then((data) => {
                const {matches, total} = data;
                const mappedData = matches.map(match => {
                    return {
                        ...match,
                        awayTeam: match.awayTeam.teamName,
                        homeTeam: match.homeTeam.teamName,
                        matchStatus: match.matchStatus.statusName,
                        date: match.date.slice(0,10)
                    }
                })
                setTableData(mappedData);
                setRowCountState(total);
                setIsLoading(false);
            })
            .catch(error => {
                console.log(`error al obtener data del api. Detalles: ${error}`);
                setIsLoading(false);
            });
    }, [pageNumber]);
 
    return (
        <div style={{ height: '100vh', width: '100%' }}>
            <div style={{ height: '600px', padding: '50px'}}>
                <h1 style={{textTransform: 'uppercase', textAlign: 'center'}}>Matches app</h1>
                <DataGrid
                    loading={isLoading}
                    rows={tableData}
                    rowCount={rowCountState}
                    columns={COLUMNS}
                    pageSize={50}
                    onPageChange={(newPage) => setPageNumber(newPage)}
                    rowsPerPageOptions={[50]}
                    paginationMode={'server'}
                    disableSelectionOnClick={true}
                    disableColumnFilter
                />
            </div>
        </div>
    );
}
