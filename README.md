# Match Football App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Instruction

### Install dependencies
git clone to the proyect, then execute in the project directory `npm install`

### Start the app

In the project directory execute `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

